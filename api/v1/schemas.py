from pydantic import BaseModel


class ShortURLRequest(BaseModel):
    origin_url: str
    custom_short_url: str | None = None


class ShortURLResponse(BaseModel):
    id: int
    origin_url: str
    short_url: str
    custom_short_url: str | None = None
