class NotValidURL(Exception):
    pass


class CustomShortURLDuplicates(Exception):
    pass


class CustomShortURLWithPrefix(Exception):
    pass


class ShortURLNotFound(Exception):
    pass
