from urllib.parse import urlparse

from sqlalchemy.exc import IntegrityError

from api.v1.exceptions import (
    CustomShortURLDuplicates,
    CustomShortURLWithPrefix,
    NotValidURL,
    ShortURLNotFound,
)
from api.v1.schemas import ShortURLRequest, ShortURLResponse
from common.database import Session
from common.settings import settings
from urlshortener.base62 import Base62
from urlshortener.models import URL


def create_short_url(
    url_request: ShortURLRequest, author_ip: str, author_user_agent: str | None
) -> ShortURLResponse:
    try:
        parsed_url = urlparse(url_request.origin_url)
    except Exception as e:
        raise NotValidURL() from e

    if not parsed_url.scheme:
        raise NotValidURL()

    if url_request.custom_short_url and url_request.custom_short_url.startswith(
        settings.SHORT_URL_PREFIX
    ):
        raise CustomShortURLWithPrefix()

    db_url = URL(
        origin_url=url_request.origin_url,
        custom_short_url=url_request.custom_short_url,
        author_ip=author_ip,
        author_user_agent=author_user_agent,
    )

    with Session() as session:
        session.add(db_url)
        try:
            session.commit()
        except IntegrityError as e:
            if 'duplicate key value' in str(e):
                raise CustomShortURLDuplicates() from e

        session.refresh(db_url)

    return ShortURLResponse(
        id=db_url.id,
        origin_url=db_url.origin_url,
        short_url=settings.SHORT_URL_PREFIX
        + Base62.encode(db_url.id + settings.SHORT_URL_START_POSITION),
        custom_short_url=db_url.custom_short_url,
    )


def get_origin_url(short_url: str) -> str:
    db_url_id = None

    cleared_short_url = short_url.removeprefix(settings.SHORT_URL_PREFIX)
    if short_url != cleared_short_url:
        try:
            db_url_id = Base62.decode(cleared_short_url) - settings.SHORT_URL_START_POSITION
        except Exception as e:
            raise ShortURLNotFound() from e

    with Session() as session:
        if db_url_id is not None:
            db_url = session.query(URL).filter(URL.id == db_url_id).first()
        else:
            db_url = session.query(URL).filter(URL.custom_short_url == short_url).first()

    if db_url is None:
        raise ShortURLNotFound()

    return db_url.origin_url
