from typing import Annotated
from urllib.parse import quote

from fastapi import APIRouter, Header, HTTPException, Request
from starlette.responses import HTMLResponse

from api.v1.schemas import ShortURLRequest, ShortURLResponse
from common.settings import settings

from . import handlers
from .exceptions import (
    CustomShortURLDuplicates,
    CustomShortURLWithPrefix,
    NotValidURL,
    ShortURLNotFound,
)
from .handlers import get_origin_url

router = APIRouter(prefix='/short-url')


@router.post('/')
def create_short_url(
    url_request: ShortURLRequest,
    request: Request,
    user_agent: Annotated[str | None, Header()] = None,
) -> ShortURLResponse:
    """Short a URL."""
    try:
        return handlers.create_short_url(
            url_request,
            author_ip=request.client.host,
            author_user_agent=user_agent,
        )
    except NotValidURL as e:
        raise HTTPException(status_code=400, detail='Not a valid URL') from e
    except CustomShortURLWithPrefix as e:
        raise HTTPException(
            status_code=400,
            detail=f'Custom short URL cannot start with prefix "{settings.SHORT_URL_PREFIX}"',
        ) from e
    except CustomShortURLDuplicates as e:
        raise HTTPException(status_code=400, detail='This custom short URL already exists') from e


@router.get('/{short_url}', status_code=302)
def get(short_url: str) -> HTMLResponse:
    """Get a full URL by shorted."""
    try:
        origin_url = get_origin_url(short_url)
    except ShortURLNotFound as e:
        raise HTTPException(status_code=400, detail='Short URL not found') from e

    quoted_origin_url = quote(origin_url, safe=":/%#?=@[]!$&'()*+,;")

    with open(
        settings.ROOT_DIR / 'templates' / 'redirect.html', 'r', encoding='utf-8'
    ) as template_file:
        html_content = template_file.read()

    html_content = html_content.format(origin_url=quoted_origin_url)

    return HTMLResponse(
        content=html_content,
        status_code=302,
        headers={'Location': quoted_origin_url},
    )
