import sentry_sdk
import uvicorn
from fastapi import FastAPI

from api.v1.routes import router as v1_router
from common.settings import settings

sentry_sdk.init(
    dsn=settings.SENTRY_DSN,
    environment=settings.ENVIRONMENT,
)

app = FastAPI()

app.include_router(v1_router, prefix='/v1')


if __name__ == '__main__':
    uvicorn.run(
        app,
        host=settings.APP_HOST,
        port=settings.APP_PORT,
        access_log=True,
        log_level='info',
    )
