CODE = api common migrations/env.py tests urlshortener
TEST = pytest --verbosity=2 --strict-markers ${arg} -k "${k}"
BLACK = black --line-length 100 --target-version py311 --skip-string-normalization

.PHONY: lint
lint:
	flake8 --jobs 4 --statistics --show-source $(CODE)
	pylint --jobs 4 $(CODE)
	${BLACK} --check $(CODE)
	unify --in-place --recursive --check $(CODE)

.PHONY: format
format:
	autoflake $(CODE)
	isort $(CODE)
	${BLACK} $(CODE)
	unify --in-place --recursive $(CODE)

.PHONY: check
check: format lint test

.PHONY: check_ci_job
check_ci_job: lint test

.PHONY: test
test:
	${TEST} --cov=.

.PHONY: test-fast
test-fast:
	${TEST} --exitfirst --cov=.

.PHONY: test-failed
test-failed:
	${TEST} --last-failed
