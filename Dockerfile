FROM python:3.11 as base
EXPOSE 8080
WORKDIR /app
COPY pyproject.toml poetry.lock ./
RUN pip install poetry --no-cache-dir && \
    poetry config virtualenvs.create false && \
    poetry install --no-dev --no-root --no-cache --no-interaction

FROM base as production

COPY api ./api
COPY migrations ./migrations
COPY common ./common
COPY templates ./templates
COPY urlshortener ./urlshortener
COPY alembic.ini entrypoint.sh app.py ./

ENTRYPOINT ["sh", "entrypoint.sh"]

FROM base as development
RUN poetry install --no-root --no-cache --no-interaction --with dev

COPY api ./api
COPY migrations ./migrations
COPY common ./common
COPY templates ./templates
COPY urlshortener ./urlshortener
COPY tests ./tests
COPY alembic.ini entrypoint.sh app.py Makefile .pylintrc setup.cfg ./

CMD ["make", "check_ci_job"]
