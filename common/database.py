from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

from common.settings import settings

engine = create_engine(settings.DATABASE_URI)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
