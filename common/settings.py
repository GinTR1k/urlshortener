from pathlib import Path

from pydantic import BaseSettings

ROOT_DIR = Path(__file__).parent.parent


class Settings(BaseSettings):
    ROOT_DIR = ROOT_DIR

    APP_HOST: str = '0.0.0.0'
    APP_PORT: int = 8080

    DATABASE_URI: str

    SENTRY_DSN: str | None = None
    ENVIRONMENT: str = 'development'

    # For example, short ULR would be like '/R123'
    SHORT_URL_PREFIX: str = 'R'
    # In numbers
    SHORT_URL_START_POSITION: int = 238327

    class Config:
        env_file = ROOT_DIR / '.env'
        env_file_encoding = 'utf-8'


settings = Settings()
