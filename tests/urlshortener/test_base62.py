import pytest

from urlshortener.base62 import Base62


@pytest.mark.parametrize(
    'number, expected',
    (
        (0, '0'),
        (1, '1'),
        (100, '1C'),
        (999999999999, 'hBxM5A3'),
        (-1, '-1'),
    ),
)
def test_encode(number, expected):
    assert Base62.encode(number) == expected


@pytest.mark.parametrize(
    'number, expected_exception',
    (
        (1.0, AssertionError),
        (1.1, AssertionError),
    ),
)
def test_encode_with_exceptions(number, expected_exception):
    with pytest.raises(expected_exception):
        Base62.encode(number)


@pytest.mark.parametrize(
    'value, expected',
    (
        ('0', 0),
        ('1', 1),
        ('1C', 100),
        ('hBxM5A4', 1_000_000_000_000),
        ('-1', -1),
        ('1000', 238328),
    ),
)
def test_decode(value, expected):
    assert Base62.decode(value) == expected


@pytest.mark.parametrize(
    'value, expected_exception',
    (
        ('', AssertionError),
        ('-', AssertionError),
        ('--', AssertionError),
        ('1-', AssertionError),
        ('123.', AssertionError),
    ),
)
def test_decode_with_exceptions(value, expected_exception):
    with pytest.raises(expected_exception):
        Base62.decode(value)
