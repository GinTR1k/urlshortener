import pytest
from alembic import command
from alembic.config import Config
from sqlalchemy import create_engine

from common import database
from common.settings import settings


@pytest.fixture(scope='session', autouse=True)
def fake_db_engine():
    settings.DATABASE_URI = 'sqlite:///:memory:'
    database.engine = create_engine(settings.DATABASE_URI)
    database.Session.configure(bind=database.engine)


@pytest.fixture(autouse=True)
def apply_migrations():
    alembic_cfg = Config(str(settings.ROOT_DIR / 'alembic.ini'))
    alembic_cfg.set_main_option('script_location', str(settings.ROOT_DIR / 'migrations'))

    command.downgrade(alembic_cfg, 'base')
    command.upgrade(alembic_cfg, 'head')

    yield

    command.downgrade(alembic_cfg, 'base')
