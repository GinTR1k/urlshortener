import pytest

from api.v1.handlers import create_short_url
from api.v1.schemas import ShortURLRequest, ShortURLResponse
from common.database import Session
from common.settings import settings
from urlshortener.models import URL


@pytest.fixture
def freeze_url_prefix_setting():
    settings.SHORT_URL_PREFIX = 'R'


@pytest.fixture
def freeze_url_start_position_setting():
    settings.SHORT_URL_START_POSITION = 100


@pytest.mark.usefixtures('freeze_url_prefix_setting', 'freeze_url_start_position_setting')
@pytest.mark.parametrize(
    "origin_url, custom_short_url, expected_short_url",
    (
        ("https://example.com", None, 'R1D'),
        ("wiki.domain", "custom", 'R1D'),
    ),
)
def test_create_short_url_success(origin_url, custom_short_url, expected_short_url):
    url_request = ShortURLRequest(origin_url=origin_url, custom_short_url=custom_short_url)
    author_ip = "1.2.3.4"
    author_user_agent = "Test User Agent"

    response = create_short_url(url_request, author_ip, author_user_agent)

    assert isinstance(response, ShortURLResponse)
    assert response.origin_url == url_request.origin_url

    with Session() as session:
        db_urls = session.query(URL).filter_by(id=response.id).all()

    assert len(db_urls) == 1
    db_url = db_urls[0]

    assert db_url.origin_url == url_request.origin_url
    assert db_url.custom_short_url == url_request.custom_short_url
    assert db_url.author_ip == author_ip
    assert db_url.author_user_agent == author_user_agent

    assert response.short_url == expected_short_url
