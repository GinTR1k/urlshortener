import string

__all__ = ('Base62',)


class Base62:
    BASE = string.digits + string.ascii_letters

    @classmethod
    def encode(cls, number: int) -> str:
        """Encode a positive number into Base X and return the string."""
        if number == 0:
            return cls.BASE[0]

        assert isinstance(number, int), 'Number must be int type'

        negative_prefix = '-' if number < 0 else ''
        number = abs(number)

        result_list = []
        base = len(cls.BASE)

        while number:
            number, rem = divmod(number, base)
            result_list.append(cls.BASE[rem])

        result = negative_prefix + ''.join(reversed(result_list))

        return result

    @classmethod
    def decode(cls, value: str) -> int:
        """Decode a Base X encoded string into the number."""
        is_negative = value.startswith('-')
        value = value.removeprefix('-')

        assert value, 'Value cannot be empty'

        base = len(cls.BASE)
        number = 0

        idx = 0
        for char in value:
            assert char in cls.BASE, 'Allowed only letters and digits'

            power = len(value) - (idx + 1)
            number += cls.BASE.index(char) * (base**power)
            idx += 1

        return -number if is_negative else number
