from datetime import datetime

from sqlalchemy import Boolean, Column, DateTime, Integer, String

from common.database import Base


class URL(Base):
    __tablename__ = 'urls'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=datetime.utcnow, nullable=False)

    origin_url = Column(String, nullable=False)
    custom_short_url = Column(String, unique=True, index=True)
    is_active = Column(Boolean, default=True, nullable=False)

    author_ip = Column(String)
    author_user_agent = Column(String)
